##################################
Finish local configuration and run
##################################

When you have the dependencies installed, you proceed to execute the project, the following configuration is the same for windows as for GNU / Linux.

Every time you start your computer and you want to run the apache server, you must run in GNU/Linux::

	sudo /opt/lampp/lampp start
	
In windows you must run the program xampp control panel and start all services.

First time configuration
************************
.. note:: 
	This procedure should only be done once
	
When xampp is running, we must write in a web browser the address http://localhost/phpmyadmin, This page is responsible for the management of SQL databases; we go to the link "databases", and in the section of creating database we write "morphological" without quotes, and press the create button, with this we already have the database created, we can close the configuration tab of the database.

After creating the database, we proceed to generate the migration of the structures of the tables, for that we go to the project folder "morph", there we execute the next commands::

	python3 manage.py makemigrations
	python3 manage.py migrate

.. note:: 
	If all goes well, error messages should not appear

Now it's time to generate a user as administrator::

	python3 manage.py createsuperuser

Data will be requested such as:

*	username
*	email
*	password

At this point, the application is already successfully configured

Execution
*********
Every time you want to start the application, you must execute the following command in the "morph" project folder::

	python3 manage.py runserver

And you must go to the default URL that is http://127.0.0.1:8000 there you will see the application running and you can igresar with your username and password that you generated in the previous step...

Running as local server
***********************
If you want to have a computer as a local server to generate a local network around the application you must connect to any WiFi network just like the hosts that wish to access the application, there you must know the IP address assigned to you, to it runs on GNU/Linux::

	ifconfig

In Windows::

	ipconfig

there appears the assigned IP address, copy it and every time you want to run the server simply execute::

	python3 manage.py runserver <your-ip>:8000

The other hosts must access the web browser and type `http://<your-ip>:8000 <#>`_ and you're done, you have your application configured successfully








