############################
Hosting server configuration
############################

There are several hosting service providers that support django, in this documentation it has been decided to explain how a project is configured in a linux shared hosting in A2hosting since the configuration is very similar in all hostings that support django, in addition A2hosting has been tested several times and has been seen its efficiency.

Initially in the case of A2hosting, we must go to CPanel, there we go to the "Setup python application" section, there we select version 3.7 of python, the field of "App directory" we type morph and the field "App domain name" we left it empty and press the Setup button, You already have your python application created.

Now from your computer connect to the server by ssh, the password and the user are provided in the client area of a2hosting, go to the folder where you created your python application, and enter the created virtual environment (it can be viewed in the cpanel> "Setup python application" with the name "Command for entering to virtual environment"), por example: *source /home/user/virtualenv/myapp/3.6/bin/activate*

First time configuration
************************
The following steps should only be done once.

The initial installation of django and its dependencies is shown in the `following link <https://www.a2hosting.com.co/kb/developer-corner/python/installing-and-configuring-django-on-linux-shared-hosting>`_. provided by a2hosting, perform all the steps and you will have a django application running successfully.

Apart from the previous configuration, the dependencies of the project must be installed, for this it executes the following command::

	pip3 install django-cleanup django_select2 django-bootstrap-datepicker-plus django-ckeditor django-bootstrap4 mysqlclient openpyxl

This page is responsible for the management of SQL databases; we go to the link "databases", and in the section of creating database we write "morphological" without quotes, and press the create button, with this we already have the database created, we can close the configuration tab of the database.

Now go to the cPanel and enter the section called "phpmyadmin", After creating the database, we proceed to generate the migration of the structures of the tables, for that we go to the project folder "morph", there we execute the next commands::

	python3 manage.py makemigrations
	python3 manage.py migrate

.. note:: 
	If all goes well, error messages should not appear

Now it's time to generate a user as administrator::

	python3 manage.py createsuperuser

Data will be requested such as:

*	username
*	email
*	password

At this point, the application is already successfully configured, restart the server by clicking on the "restart" button in the cPanel section > "Setup python application" for the changes to take effect


