########################
Installation instuctions
########################

The software execution process is carried out following a series of steps and previous configurations. This manual shows the process of installing dependencies in a personal computer that can serve as a local host to generate an internal network, or installation in a paid hosting; Regarding the installation in the local server, the manual for installation in the GNU / Linux and Windows operating systems is provided.

System dependencies:

*	python >= 3.6
*	pip
*	xampp >= 7.1.28
*	python3-mysqlclient

Python dependencies:

*	:class:`django` >= 2.1
*	:class:`django-cleanup`
*	:class:`django_select2`
*	:class:`django-bootstrap-datepicker-plus`
*	:class:`django-ckeditor`
*	:class:`django-bootstrap4`
*	:class:`mysqlclient`
*	:class:`openpyxl`

The following pages explain how to install the dependencies.
