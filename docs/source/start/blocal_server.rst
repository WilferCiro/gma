##########################
Local server configuration
##########################

This section shows the process of installing the corresponding dependencies to configure the GMA software on a personal computer to generate personal networks or to create individual projects.

You have the scenarios where the following operating systems are:

*	Windows
*	GNU/Linux (Ubuntu based)
*	GNU/Linux (Arch linux based)

GNU/Linux (All distibutions) initial proccess
*********************************************
The first step is to install the LAMPP software, which allows us to install the mysql database manager in a very simple way, to do so, download the following package https://www.apachefriends.org/xampp-files/7.3.4/xampp-linux-x64-7.3.4-0-installer.run that is easily installed in GNU/Linux operating systems. If you want to know more about this project, visit the following link https://www.apachefriends.org/download.html

When this software is downloaded, we execute the following commands in the folder where the file is located::

	sudo chmod +x xampp-linux-x64-7.1.28-0-installer.run
	sudo ./xampp-linux-x64-7.1.28-0-installer.run

The program installation process is executed. To install, follow the instructions provided by the same.

Every time you start your computer and you want to run the apache server, you must run::

	sudo /opt/lampp/lampp start

The software is created with the framework called django, which has python as the programming language, we proceed to install it.

**In Ubuntu based**::

	sudo apt install python3
	sudo apt install python3-pip
	sudo apt install python3-mysqlclient

**In Arch linux based**::

	sudo pacman -S python
	sudo pacman -S python-pip
	sudo pacman -S python-mysqlclient

.. note:: 
	Many times, Linux distributions have installed the default dependencies, in this case, the system will inform you that the packages are already installed.

Now, we proceed to install django and its dependencies.

**In all GNU/Linux distibutions**::

	sudo pip3 install django django-cleanup django_select2 django-bootstrap-datepicker-plus django-ckeditor django-bootstrap4 mysqlclient openpyxl


Microsoft Windows, initial proccess
***********************************
Before installing django, it is necessary to install python next to the dependencies of the work environment, for this you must first check the Windows version, for it press the Windows key + Pause / Break, this will open the information of your system, there visualize if your version is 64 bits or 32 bits. Now with the version of your operating system, go to the next page and download the latest version of python 3 https://www.python.org/downloads/windows/, keep in mind the version of your operating system, install the program .

.. note::
	When you install the program, select the "Add python 3.X to PATH" option

Now we are going to install django and its dependencies, for it press the Windows + R keys and type cmd, there will be displayed a window known as command line, there we will execute::

	python3 -m pip install --upgrade pip

And finally we execute::

	pip install django django-cleanup django_select2 django-bootstrap-datepicker-plus django-ckeditor django-bootstrap4 mysqlclient openpyxl

The last step is to install the XAMPP software, which allows us to install the mysql database manager in a very simple way, to do so, download the following package https://www.apachefriends.org/xampp-files/7.3.4/xampp-windows-x64-7.3.4-0-VC15-installer.exe that is easily installed in Windows. If you want to know more about this project, visit the following link https://www.apachefriends.org/download.html. Install xampp and start the "Mysql" and "Apache" services, If the services are started correctly, they should appear green.





