.. morph documentation master file, created by
   sphinx-quickstart on Sat May  4 12:36:52 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to morph's documentation!
=================================

Project created at Universidad del Quindío, Colombia

.. toctree::

   Return to page <http://gma.pythonanywhere.com>

.. toctree::
	:glob:
	:numbered:
	:caption: Initial proccess
	
	start/*

.. toctree::
	:glob:
	:numbered:
	:caption: Usage
	
	usage/*

.. toctree::
	:glob:
	:caption: About the project
	
	about
	

