#############
No admin user
#############

The no admin user user profile can perform the following operations

*	Create and manage projects
*	Edit profile data

Visual structure of the program
*******************************
The web application has three main sections, these sections are composed in: left side panel which contains the main menu, the top where the information of the current page and the user options are, finally the body of the page where all the events of the application occur.

Create and manage projects
**************************
As it is said before, in the part of the left of the page is the menu, to manage the projects, go to the section called "Projects", there is a table which contains the list of the existing projects with their respective data, the data "Mode" refers to the mode of administration of the project of your profile, if you are an administrator you can edit all its characteristics, but if you are a collaborator you can only do limited things available to the administrator.

Click on the buttons to add, edit or delete projects to perform the respective actions, when you click on them, a modal window will appear asking you to add the necessary data and confirmations.

The display button takes you to a different page where the specific project data is displayed, there you can edit them and add a characteristic image of your project, you can export all the data of the project with their respective matrices by pressing the button that has the symbol of a spreadsheet, in addition, you find three additional buttons with links to the input matrix, the cross consistency matrix, and the status of problems.

Input matrix view
=================
In this section the aforementioned matrix is shown, you can add parameters and their respective values, to do so, press on the button that has the add symbol with title "Parameter", there you will be asked for the title of the parameter and a value initially, for add another value, click on the box in the parameter table and press the add button, there you will be asked for the value text. So you will proceed with the following actions.

In addition, you can give permission to collaborating users to edit only specific parameters and thus have better clarity about the process, for more information, click on the "help" button on the aforementioned page.

Cross consistency matrix
========================
In this section the consistency matrix is shown, the separate parameters are generated to improve the visualization, for each parameter the options with which it can be related are enabled, to change the status of "X" (they are not related) to "- "(if they are related) you simply have to click on the button and that's it, for more information, click on the "help" button on the aforementioned page.

Problem states
==============
This section shows the initial matrix where you can click on the cells you want, clicking on a cell will enable and disable others according to what you have done in the cross consistency matrix, the cores are Red, Blue and Gray , those that correspond to selected, enabled to select, disabled to select respectively.

When you finish the process, you can save the configuration by clicking on the "Options"> "save" button and overwrite an existing record or create a new record, which you can then load by clicking on "Options"> "load".

Edit profile data
*****************
Personal data can be edited, so go to the menu at the top, click on your photo and then click on the "Profile" option, this will show you your personal data and the different options regarding your data, for example the edition of your password, the edition of your profile photo and your data, plus a list of the projects in which you participate.

