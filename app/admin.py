from django.contrib import admin
from app.models import *
from django.contrib.auth.admin import UserAdmin

# Register your models here.
admin.site.register(Project)
admin.site.register(ProjectUser)
admin.site.register(User, UserAdmin)

admin.site.register(Country)
admin.site.register(Parameter)
admin.site.register(Value)
admin.site.register(ParameterProjectUser)


admin.site.register(ValuesCrossMatrix)
admin.site.register(CrossMatrix)

admin.site.register(SavedStatesProblem)
