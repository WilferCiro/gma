from app.models import *
from django.forms import ModelForm
from django.contrib.admin import widgets
from bootstrap_datepicker_plus import DatePickerInput
from django import forms

class formUser(ModelForm):
	
	class Meta:
		model = User
		fields = ["last_name", "first_name", "born_date", "cellphone", "email", "home_city", "home_country", "profession", "observations"]
		
		widgets = {
			'born_date': DatePickerInput(options={"format":"YYYY-MM-DD"}),
		}

class editUser(ModelForm):
	class Meta:
		model = User
		fields = ["last_name", "first_name", "born_date", "cellphone", "email", "home_city", "home_country", "profession", "locked"]
		
		widgets = {
			'born_date': DatePickerInput(options={"format":"YYYY-MM-DD"}),
		}

class editUserProfile(ModelForm):
	class Meta:
		model = User
		fields = ["last_name", "first_name", "born_date", "cellphone", "email", "profession"]
		
		widgets = {
			'born_date': DatePickerInput(options={"format":"YYYY-MM-DD"}),
		}

class changePass(forms.Form):
	pass1 = forms.CharField(widget=forms.PasswordInput())
	pass2 = forms.CharField(widget=forms.PasswordInput())


class addUser(ModelForm):
	class Meta:
		model = User
		fields = ["username", "last_name", "first_name", "born_date", "cellphone", "email", "home_city", "home_country", "profession", "locked"]
		
		widgets = {
			'born_date': DatePickerInput(options={"format":"YYYY-MM-DD"}),
		}
		
class editAvatar(ModelForm):
	class Meta:
		model = User
		fields = ["avatar"]
