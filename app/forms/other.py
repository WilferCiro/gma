from app.models import *
from django.forms import ModelForm

class editIndex(ModelForm):
	
	class Meta:
		model = IndexConfiguration
		fields = "__all__"
