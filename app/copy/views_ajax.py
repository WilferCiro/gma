from app.models import *
from django.http import JsonResponse
from django.views import View
from django.db.models import Q


class UserListAjax(View):
	def get(self, request, *args, **kargs):
		term = request.GET.get("term", "")
		users = User.objects.filter((Q(last_name__icontains = term) | Q(first_name__icontains = term) | Q(username__icontains = term)) and ~Q(pk = request.user.pk))[0:10]
		in_data = []
		for ci in users:
			in_data.append({"text" : ci.first_name + " " + ci.last_name + " - " + str(ci.username), "id" : ci.pk})
		data = {
			'results': in_data
		}
		return JsonResponse(data)

