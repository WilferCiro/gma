from .views_main import Main, MainTable
from django.views import View
from app.models import *

from django.db.models import Q
from django.conf import settings

from .form_project import *
from django.http import JsonResponse
from django.shortcuts import HttpResponseRedirect
from types import *


class ProjectsView(MainTable, View):
	"""
		view of projects page
	"""
	page_title = "Project list"
	register_name = "project"	
	form_action = "/projects/"	
	model_object = Project
	table_columns = dict(title = "Title", observations = "Observations", modification_date__date = "Modification date", mode__format = "Mode", keywords = "Keywords", is_public = "Public")
	return_edit_columns = ["title", "observations", "keywords", "is_public"]
	form_edit = editProject
	form_add = editProject
	delete_register = True
	can_view = True
	view_aparte = True
	
	
	def _addData(self, request):
		"""
			Add a table register
		"""
		if self.can_add:
			form_edit = self.form_edit(data = request.POST)
			if form_edit.is_valid():
				new_element = form_edit.save()
				new_pu = ProjectUser(user = request.user, project = new_element, mode = ProjectUser.ADMIN)
				new_pu.save()
				
				return True
			request.session['last_error'] = form_edit.errors
		else:
			request.session['last_error'] = "Usted no tiene permisos para añadir este objeto"
		return False
	
	
	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Home", "/")
		self._add_breadcrumb("Projects list", "")
	
	def _getFilerTable(self, value, user):
		fil = Q(pk = None)
		pu = ProjectUser.objects.filter(user = user)
		if len(pu)>0:
			fil = Q()
		for p in pu:
			fil = fil | Q(pk = p.project.pk)
		
		if value != "":
			fil = fil & (Q(title__icontains = value) | Q(observations__icontains = value) | Q(keywords__icontains=value) | Q(modification_date__icontains = value))
			return fil
		return fil
	
	def can_edit_register(self, obj, user):		
		pu = ProjectUser.objects.filter(Q(user = user) & Q(project = obj) & Q(mode = ProjectUser.ADMIN))
		if len(pu) > 0:
			return True
		return False
	
	def can_delete_register(self, obj, user):		
		return self.can_edit_register(obj, user)
		
		
	def _preProccess(self, request):
		user = request.user
		self.can_edit = True
		self.can_delete = True
		self.can_add = True
	
	def _getFormatRow(self, first, e, user):
		if first == "mode":
			pu = ProjectUser.objects.get(Q(user = user) & Q(project = e))
			return pu.mode
		return ""
		

class ProjectProfileView(Main, View):
	#TODO: add historic
	template = 'project_profile.html'
	page_title = "Project data"
	
	def get_data(self, request, kwargs):	
		self._add_breadcrumb("Home", "/")
		self._add_breadcrumb("Projects", "/projects")
		self._add_breadcrumb("Project data", "")
		
		self._add_js("project_user.js")
		pk = int(kwargs['pk'])
		self.pk = pk
		try:
			project = Project.objects.get(pk = pk)
			pu = ProjectUser.objects.get(user = request.user, project = project)
		except Exception as ex:
			print(ex)
			self.can_view_page = False
			return {}
		form_edit = editProject(instance = project)
		is_project_admin = False
		if pu.mode == ProjectUser.ADMIN:
			is_project_admin = True
		
		pro_user = []
		for project_users in ProjectUser.objects.filter(Q(project = project) & ~Q(user = request.user)):
			pro_user.append(dict(user = project_users.user.get_full_name(), mode = project_users.mode, pk = project_users.pk))
		
		form_action = request.path
		form_add = userAdd()
		return {"form_add" : form_add, "form_action" : form_action, "form_edit" : form_edit, "project" : project, "is_project_admin" : is_project_admin, "pro_user" : pro_user}
	
	def _checkPermission(self, object_id, user):
		try:
			project = Project.objects.get(pk = object_id)
			pu = ProjectUser.objects.get(user = user, project = project)
		except Exception as ex:
			print(ex)
			return False
		if pu.mode == ProjectUser.ADMIN:
			return True
		return False
	
	def _editData(self, request):
		"""
			Edit the object data depending of the form
		"""
		object_id = request.POST.get("object_id", None)
		if object_id != None:
			if self._checkPermission(object_id, request.user):
				sed = Project(pk = object_id)
				form_edit = editProject(data = request.POST, instance=sed)
				if form_edit.is_valid():
					form_edit.save()	
					return True
				request.session['last_error'] = form_edit.errors
		else:
			request.session['last_error'] = "Permission"
		return False
	
	def _addData(self, request):
		"""
			Add a user
		"""
		object_id = request.POST.get("object_id", None)
		mode = request.POST.get("mode", None)
		user_new = request.POST.get("user", None)
		if object_id != None and mode != None and user_new != None and str(user_new) != str(request.user.pk):
			if self._checkPermission(object_id, request.user):
				project = Project.objects.get(pk = object_id)
				user_n = User.objects.get(pk = user_new)
				try:
					pu_Exists = ProjectUser.objects.get(user = user_n, project = project)
					return False
				except:					
					pu = ProjectUser(project = project, mode = mode, user = user_n)
					pu.save()
					return True
		return False

	def _deleteData(self, request):
		"""
			Deletes a table register
		"""
		object_id = request.POST.get("object_id", None)
		project_id = request.POST.get("project_id", None)
		if object_id != None and project_id != None:
			if self._checkPermission(project_id, request.user):
				sed = ProjectUser.objects.get(pk = object_id)
				sed.delete()
				return True
		return False


class ProjectInputMatrixView(Main, View):
	#TODO: add historic
	template = 'project/input_matrix.html'
	page_title = "Input matrix"
	
	def getTableStructure(self, project_id, user, is_project_admin):
		parameters = Parameter.objects.filter(project = project_id).order_by("order")
		parameter_model = []
		for col in parameters:
			can_edit = True
			can_view = True
			if not is_project_admin:
				can_edit = False
				can_view = False
				try:
					pu = ProjectUser.objects.get(project = project_id, user = user)
					cpu = ParameterProjectUser.objects.get(parameter = col, project_user = pu)					
					if cpu.mode == ParameterProjectUser.M_EDIT or cpu.mode == ParameterProjectUser.M_SEE_EDIT:
						can_edit = True
					if cpu.mode == ParameterProjectUser.M_SEE or cpu.mode == ParameterProjectUser.M_SEE_EDIT:
						can_view = True
				except:
					pass
			parameter_model.append(dict(title = col.title, observations = col.observations, id = col.id, can_view = can_view, can_edit = can_edit))
		value_model = [[0] * len(parameters)]
		parameter_number = 0
		for col in parameter_model:
			value_number = 0
			values = Value.objects.filter(parameter = col["id"]).order_by("order")
			for ro in values:
				if value_number+1 > len(value_model):
					value_model.append([0] * len(parameters))
				title = ro.title
				if title == "":
					title = None
				value_model[value_number][parameter_number] = dict(title = title, exists = True, observations = ro.observations, id = ro.id, col_id = col["id"], can_view = col["can_view"], can_edit = col["can_edit"])
				
				value_number = value_number + 1
			parameter_number = parameter_number + 1
		return value_model, parameter_model
	
	def _getUserParameter(self, user, project):
		data = []
		for project_users in ProjectUser.objects.filter(Q(project = project) & ~Q(user = user) & Q(mode = ProjectUser.COLLABORATOR)):
			parameters = Parameter.objects.filter(project = project).order_by("order")
			for col in parameters:				
				try:
					cpu = ParameterProjectUser.objects.get(project_user = project_users, parameter = col)
				except:
					cpu = ParameterProjectUser(project_user = project_users, parameter = col)
					cpu.save()
				interna = dict()
				interna["user"] = project_users.user
				interna["parameter"] = str(col.title)
				interna["pk"] = cpu.pk
				can_edit = False
				can_view = False
				if cpu.mode == ParameterProjectUser.M_EDIT or cpu.mode == ParameterProjectUser.M_SEE_EDIT:
					can_edit = True
				if cpu.mode == ParameterProjectUser.M_SEE or cpu.mode == ParameterProjectUser.M_SEE_EDIT:
					can_view = True
				interna["can_edit"] = can_edit	
				interna["can_view"] = can_view
				data.append(interna)
		return data
		
	
	def get_data(self, request, kwargs):
		pk = int(kwargs['pk'])
		self.pk = pk
		try:
			project = Project.objects.get(pk = pk)
		except Exception as ex:
			self.can_view_page = False
			return {}
			
		is_project_admin = False
		can_edit = False
		try:
			pu = ProjectUser.objects.get(user = request.user, project = project)
			if pu.mode == ProjectUser.ADMIN:
				is_project_admin = True
			can_edit = True
		except:
			if not project.is_public == Project.PUBLIC:
				self.can_view_page = False
				return {}				
		
		self._add_breadcrumb("Home", "/")
		self._add_breadcrumb("Projects", "/projects")
		self._add_breadcrumb(project.title, "/projects/view/"+str(pk))
		self._add_breadcrumb("Input matrix", "")
		
		self._add_js("input_matrix.js")
		self._add_css("input_matrix.css")
			
		user_permissions = self._getUserParameter(request.user, project)
		table_model, parameter_model = self.getTableStructure(project, request.user, is_project_admin)
		
		form_action = request.path
		data = dict()
		data["project"] = project
		data["is_project_admin"] = is_project_admin
		data["table_model"] = table_model
		data["parameter_model"] = parameter_model
		data["can_edit"] = can_edit
		data["form_action"] = form_action
		data["user_permissions"] = user_permissions
		return data
	
	def _checkPermission(self, object_id, user):
		try:
			project = Project.objects.get(pk = object_id)
			pu = ProjectUser.objects.get(user = user, project = project)
		except Exception as ex:
			print(ex)
			return False
		if pu.mode == ProjectUser.ADMIN:
			return True
		return False
	
	def _checkPermissionParameter(self, project_id, parameter_id, user):
		try:
			project = Project.objects.get(pk = project_id)
			pu = ProjectUser.objects.get(user = user, project = project)
		except Exception as ex:
			print(ex)
			return False
		if pu.mode == ProjectUser.ADMIN or (pu.mode == ProjectUser.COLLABORATOR and self._canModifyParameter(parameter_id, user)):
			return True
		return False
	
	def _canModifyParameter(self, parameter_id, user):
		return True
	
	def _addData(self, request):
		"""
			Add a register
		"""
		tipoAdd = request.POST.get("tipoAdd", None)
		if tipoAdd == "parameter":
			object_id = request.POST.get("object_id", None)
			if object_id != None:
				if self._checkPermission(object_id, request.user):
					project = Project.objects.get(pk = object_id)
					title = request.POST.get("title_parameter", "Title")
					title_value = request.POST.get("title_value", "Title")
					parameter = Parameter(title = title, project = project, order='0')
					parameter.save()
					fila = Value(title = title_value, parameter = parameter, order = '0')
					fila.save()
					return True
		elif tipoAdd == "value":
			parameter_id = request.POST.get("parameter_id", None)
			project_id = request.POST.get("project_id", None)
			if project_id != None and parameter_id != None:
				if self._checkPermissionParameter(project_id, parameter_id, request.user):
					title_value = request.POST.get("title_value", "Title")
					parameter = Parameter.objects.get(pk = int(parameter_id))
					fila = Value(title = title_value, parameter = parameter, order = '0')
					fila.save()
					return True
		return False
		
	def _editData(self, request):
		"""
			Add a register
		"""
		tipoEdit = request.POST.get("tipoEdit", None)
				
		if tipoEdit == "edit_value":			
			value = self._valueSecurity(request)
			if value != None:
				title = request.POST.get("title", "title")
				observations = request.POST.get("observations", "observations")
				value.title = title
				value.observations = observations
				value.save()
				return True
		
		elif tipoEdit == "edit_parameter":
			parameter = self._parameterSecurity(request)
			if parameter != None:
				title = request.POST.get("title", "title")
				observations = request.POST.get("observations", "observations")
				parameter.title = title
				parameter.observations = observations
				parameter.save()
				return True
		
		elif tipoEdit == "permission":
			project_id = request.POST.get("project_id", None)
			pk = request.POST.get("pk", None)
			if project_id != None and pk != None:
				if self._checkPermission(project_id, request.user):
					try:
						can_edit = request.POST.get("can_edit", "false")
						can_view = request.POST.get("can_view", "false")
						print(can_edit)
						print(can_view)
						if can_edit == "true" and can_view == "true":
							mode = ParameterProjectUser.M_SEE_EDIT
						elif can_edit == "true" and can_view == "false":
							mode = ParameterProjectUser.M_EDIT
						elif can_edit == "false" and can_view == "true":
							mode = ParameterProjectUser.M_SEE
						else:
							mode = ParameterProjectUser.M_NONE
						cpu = ParameterProjectUser.objects.get(pk = pk)
						cpu.mode = mode
						cpu.save()
					except:
						pass
		return False
	
	def _deleteData(self, request):
		"""
			Add a register
		"""
		tipoEdit = request.POST.get("tipoDelete", None)
		if tipoEdit == "parameter":
			parameter = self._parameterSecurity(request)
			if parameter != None:
				parameter.delete()
				return True
					
		elif tipoEdit == "value":
			value = self._valueSecurity(request)
			if value != None:
				value.delete()
				return True
		return False
	
	
	def _parameterSecurity(self, request):
		parameter_id = request.POST.get("parameter_id", None)
		project_id = request.POST.get("project_id", None)
		if project_id != None and parameter_id != None:
			if self._checkPermissionParameter(project_id, parameter_id, request.user):
				parameter = Parameter.objects.get(pk = int(parameter_id))
				return parameter
		return None
	
	def _valueSecurity(self, request):
		value_id = request.POST.get("value_id", None)
		parameter_id = request.POST.get("parameter_id", None)
		project_id = request.POST.get("project_id", None)
		if project_id != None and parameter_id != None and value_id != None:
			if self._checkPermissionParameter(project_id, parameter_id, request.user):
				value = Value.objects.get(pk = int(value_id))
				parameter = Parameter.objects.get(pk = int(parameter_id))
				if value.parameter == parameter:
					return value
		return None
		
	def _ajaxRequest(self, request):
		action = request.POST.get("ajax_action", None)
		error = False
		if action == "editData":
			error = self._editData(request)
		error = not(error)	
		data = {
			'error': error
		}
		return JsonResponse(data)
		


class ProjectCrossMatrix(Main, View):
	#TODO: add historic
	template = 'project/cross_matrix.html'
	page_title = "Cross matrix"
	
	def getTableStructure(self, project):
		parameters = Parameter.objects.filter(project = project).order_by("order")
		parameter_model = []
		for col in parameters:
			values = Value.objects.filter(parameter = col.id).order_by("order")
			parameter_values = []
			for ro in values:
				parameter_values.append({
					"title" : ro.title,
					"observations" : ro.observations,
					"pk" : ro.id
				})
			
			parameter_model.append({
					"title" : col.title,
					"observations" : col.observations,
					"pk" : col.id,
					"values" : parameter_values
				})
		tablas = []
		var = 1
		for nro in parameter_model:
			intern_table = []
			titles_table = []
			param_left = nro["title"]
			for body in parameter_model[var-1:var]:
				for row_1 in body["values"]:
					fila = {
						"title" : row_1["title"],
						"valores" : []
					}
					for title in parameter_model[0:var-1]:
						if title not in titles_table:
							titles_table.append(title)
						for row_2 in title["values"]:
							try:
								value_bd = CrossMatrix.objects.get(Q(value1__pk = row_1["pk"]) & Q(value2__pk = row_2["pk"]))
							except CrossMatrix.DoesNotExist:
								value_bd = CrossMatrix(value1 = Value.objects.get(pk = row_1["pk"]), value2 = Value.objects.get(pk = row_2["pk"]), project = project)
								value_bd.save()
							fila["valores"].append({
								"legend" : row_1["title"] + " - " + row_2["title"],
								"value" : value_bd.state,
								"pk" : value_bd.pk,
							})
					if fila["valores"] != []:
						intern_table.append(fila)
			if intern_table != []:
				tablas.append({
					"intern_table" : intern_table,
					"titles_table" : titles_table,
					"param_left" : param_left
				})
			var += 1
		return tablas
		"""	
		table_model = []
		
		top = []
		for col in parameter_model:
			if col != parameter_model[0]:
				value_model = []
				for ro in col["values"]:
					value_model.append(dict(title = ro["title"], value_id = ro["id"], parameter = col, length = len(col["values"])))
				top.append(value_model)
		table_model.append(top)

		top = []
		for col in parameter_model:
			if col != parameter_model[-1]:
				value_model = []
				for ro in col["values"]:
					value_model.append(dict(title = ro["title"], value_id = ro["id"], parameter = col))
				top.append(value_model)
		table_model.append(top)
		
		model_final = []
				
		for a in range(0, len(table_model[0])):
			for b in table_model[0][a]:
				mod = [b]
				for c in range(0, a+1):
					for d in table_model[1][c]:
						# TODO: change this raw
						for sql in ValuesCrossMatrix.objects.raw("SELECT * FROM app_valuescrossmatrix a, app_valuescrossmatrix b where a.value_id = '" + str(b["value_id"]) + "' AND b.value_id = '" + str(d["value_id"]) + "' AND a.crossMatrix_id = b.crossMatrix_id LIMIT 1"):
							crossMatrix_id = sql.crossMatrix
							dataCross = CrossMatrix.objects.get(pk = crossMatrix_id.id)
							if dataCross:
								mod.append(dict(state = dataCross.state, observations = dataCross.observations, relational_id = crossMatrix_id.id))
							else:			
								rcm1 = ValuesCrossMatrix(value = Value.objects.get(pk=b["value_id"]), crossMatrix = crossMatrix_id)
								rcm1.save()
								rcm2 = ValuesCrossMatrix(value = Value.objects.get(pk=d["value_id"]), crossMatrix = crossMatrix_id)
								rcm2.save()
								mod.append(dict(state = "-", observations = "", relational_id = crossMatrix_id.id))
							break
						else:
							cm = CrossMatrix(project = project_id, state="-", observations="")
							cm.save()
							rcm1 = ValuesCrossMatrix(value = Value.objects.get(pk=b["value_id"]), crossMatrix = cm)
							rcm1.save()
							rcm2 = ValuesCrossMatrix(value = Value.objects.get(pk=d["value_id"]), crossMatrix = cm)
							rcm2.save()
							mod.append(dict(state = "-", observations = "", relational_id = cm.id))							
				model_final.append(mod)
		
		return model_final, parameter_model"""
	
	
	def get_data(self, request, kwargs):
		pk = int(kwargs['pk'])
		try:
			project = Project.objects.get(pk = pk)
		except Exception as ex:
			self.can_view_page = False
			return {}
			
		is_project_admin = False
		can_edit = False
		try:
			pu = ProjectUser.objects.get(user = request.user, project = project)
			if pu.mode == ProjectUser.ADMIN:
				is_project_admin = True
				can_edit = True
		except:
			if not project.is_public == Project.PUBLIC:
				self.can_view_page = False
				return {}	
							
		table_model = self.getTableStructure(project)
		self._add_breadcrumb("Home", "/")
		self._add_breadcrumb("Projects", "/projects")
		self._add_breadcrumb(project.title, "/projects/view/"+str(pk))
		self._add_breadcrumb("Cross matrix", "")
		
		self._add_js("project_coss_matrix.js")
		self._add_css("cross_matrix.css")
		
		data = dict()
		data["table_model"] = table_model
		#data["parameter_model"] = parameter_model
		data["can_edit"] = can_edit
		data["project"] = project
		data["is_project_admin"] = is_project_admin
		data["form_action"] = request.path
		
		return data
	
	def _checkPermissionCross(self, project_id, parameter_id, user):
		try:
			project = Project.objects.get(pk = project_id)
			pu = ProjectUser.objects.get(user = user, project = project)
		except Exception as ex:
			print(ex)
			return False
		if pu.mode == ProjectUser.ADMIN:
			return True
		return False
	
	def _crossSecurity(self, request):
		cross_id = request.POST.get("cross_id", None)
		project_id = request.POST.get("project_id", None)
		if project_id != None and cross_id != None:
			if self._checkPermissionCross(project_id, cross_id, request.user):
				cross = CrossMatrix.objects.get(pk = int(cross_id))
				return cross
		return None
		
	
	def _editData(self, request):
		"""
			Add a register
		"""
		tipoEdit = request.POST.get("tipoEdit", None)
		if tipoEdit == "cross_observations":			
			value = self._crossSecurity(request)
			if value != None:
				observations = request.POST.get("observations", "None")
				value.observations = observations
				value.save()
				return True
		
		elif tipoEdit == "state":
			value = self._crossSecurity(request)	
			if value != None:
				state = request.POST.get("state", "-")
				value.state = state
				value.save()
				return True
		
		return False

	def _ajaxRequest(self, request):
		action = request.POST.get("ajax_action", None)
		error = False
		if action == "editData":
			error = self._editData(request)
		error = not(error)	
		data = {
			'error': error
		}
		return JsonResponse(data)


class ProjectStatesView(Main, View):
	template = 'project/problem_states.html'
	page_title = "Problem states"

	def get_data(self, request, kwargs):
		pk = int(kwargs['pk'])
		try:
			project = Project.objects.get(pk = pk)
		except Exception as ex:
			self.can_view_page = False
			return {}
			
		is_project_admin = False
		can_edit = False
		try:
			pu = ProjectUser.objects.get(user = request.user, project = project)
			if pu.mode == ProjectUser.ADMIN:
				is_project_admin = True
				can_edit = True
		except:
			if not project.is_public == Project.PUBLIC:
				self.can_view_page = False
				return {}	
		
		saved_states = request.GET.get("saved_states", None)
		state = None
		if saved_states != None:
			try:
				state = SavedStatesProblem.objects.get(project = project, pk = saved_states)
			except:
				pass
		
		user_permissions = ProjectInputMatrixView._getUserParameter(self, request.user, project)
		table_model, parameter_model = ProjectInputMatrixView.getTableStructure(self, project, request.user, is_project_admin)							

		self._add_breadcrumb("Home", "/")
		self._add_breadcrumb("Projects", "/projects")
		self._add_breadcrumb(project.title, "/projects/view/"+str(pk))
		self._add_breadcrumb("Problem states", "")
		
		self._add_js("project_states.js")
		self._add_js("html2canvas.js")
		self._add_css("project_states.css")
		
		form_saved = SavedStates(project_id = project)
		
		data = dict()
		data["can_edit"] = can_edit
		data["project"] = project
		data["is_project_admin"] = is_project_admin
		data["form_action"] = request.path
		data["table_model"] = table_model
		data["parameter_model"] = parameter_model
		data["form_action"] = request.path
		data["form_saved"] = form_saved
		if state != None:
			data["state"] = state.model
		else:
			data["state"] = ""
		
		return data
	
	def _requestData(self, request):
		data_return = []
		data_in = []
		data_get = request.POST.get("pk")
		first = False
		if data_get != "":
			for field in data_get.split(","):
				if field != "":
					Row = Value.objects.get(pk = int(field))
					data_in = []
					for relation in CrossMatrix.objects.filter((Q(value1 = Row) | Q(value2 = Row)) & Q(state = "-")):
						#for val in ValuesCrossMatrix.objects.filter(Q(crossMatrix = relation.crossMatrix) & Q(crossMatrix__state = "-")):
						value1 = str(relation.value1.pk)
						value2 = str(relation.value2.pk)
						if value1 not in data_return and value1 not in field:
							data_in.append(value1)							
						if value2 not in data_return and value2 not in field:
							data_in.append(value2)
					data_return.append(data_in)
			data_in = []
			for i in data_return[0]:
				count = 0
				for data in data_return[1:]:
					if i in data:
						count += 1
				if count >= len(data_return[1:]):
					data_in.append(i)
	
		return {'values' : ",".join(data_in)}
	
	def _ajaxRequest(self, request):
		action = request.POST.get("ajax_action", None)
		json_data = None
		error = True
		if action == "requestData":
			json_data = self._requestData(request)
		elif action == "editData":
			json_data = self._editData(request)
		if json_data == None:
			json_data = {"error" : not(error)}
		
		return JsonResponse(json_data)
	
	def _editData(self, request):
		pk = request.POST.get("pk", "")
		project_id = request.POST.get("project_id", None)
		action = request.POST.get("action", None)
		new_name = request.POST.get("new_name", None)
		if project_id != None and action != None and new_name != None:
			try:
				project = Project.objects.get(pk = project_id)
			except Exception as ex:
				return False
			if action == "save_new":
				new_reg = SavedStatesProblem(project = project, model = pk, name = new_name, observations = "")
				new_reg.save()
				return {"pk" : new_reg.pk}		
			elif action == "save_over":
				try:
					reg = SavedStatesProblem.objects.get(pk = new_name)
				except:
					return False
				reg.model = pk
				reg.save()
				return {"pk" : reg.pk}
		return False
		
	
	
	
	
	
	
	
	
