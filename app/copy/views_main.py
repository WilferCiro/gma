from django.shortcuts import render
from django.views import View
from app.models import *
from django.http import JsonResponse
from django.db.models import Q
from django.shortcuts import HttpResponseRedirect
import os.path
from django.conf import settings
from django.utils.formats import localize, date_format

class Main(object):
	"""
		Main Class
	"""
	template = "structure.html"
	menu = []
	js_files = []
	css_files = []
	breadcrumb = []
	page_title = "GMA"
	aditional_templates = []
	can_view_page = True
	
	def __init__(self):
		"""
			Constructor of the main class
		"""
		self.__empty_all()
	
	def __get_menu(self, user):
		"""
			Set the menu of application
			@param user with the user data
		"""
		menu = []
		menu.append(dict(label = "Projects", url = "/projects", icon = "fa fa-list", item_class = "active", submenu = [], content="table, list, public, pivate, cross, states"))
		if user.is_superuser:
			menu.append(dict(label = "Users", url = "/users", icon = "fa fa-users", item_class = "active", submenu = [], content="personal data, list, table"))
			menu.append(dict(label = "Index config", url = "/index/config", icon = "fa fa-edit", item_class = "active", submenu = [], content="edit"))
		menu.append(dict(label = "API usage", url = "/api", icon = "fa fa-code", item_class = "active", submenu = [], content=""))
		
		
		self.menu = menu
	
	def __empty_all(self):
		"""
			Prevent data repeat
		"""
		self.js_files = []
		self.css_files = []
		self.aditional_templates = []
		self.breadcrumb = []
		self.menu = []
	
	def _add_css(self, css_file):
		"""
			Adds a css file to document
			@param css_file as css file path inside static/
		"""
		if css_file not in self.css_files:
			self.css_files.append(css_file)
		
	def _add_js(self, js_file):
		"""
			Adds a js file to document
			@param js_file as js file path inside static/
		"""
		if js_file not in self.js_files:
			self.js_files.append(js_file)
	
	
	def _add_aditional_template(self, template):
		"""
			Adds a template for include to document
			@param template as html file path inside static/
		"""
		if template not in self.aditional_templates:
			self.aditional_templates.append(template)
	
	def _add_breadcrumb(self, label, url):
		"""
			Adds a item of the breadcrumb
			@param label as text to show, string
			@param url as url to redirect
		"""
		self.breadcrumb.append(dict(label = label, url = url))
	
	def __initialProccess(self, request, kwargs):
		"""
			Organices the initial data
		"""
		user = request.user	
		
		data_page = dict()
		if user.is_authenticated is False:
			success = request.GET.get("success", None)
			error = request.GET.get("error", None)
			self.template = "login.html"
			return dict(success = success, error = error)
		else:
			self.__get_menu(user)
			data_page = self.get_data(request, kwargs)
			data_page["menu"] = self.menu
			data_page["breadcrumb"] = self.breadcrumb
			data_page["page_title"] = self.page_title
			data_page["user"] = user
			data_page["aditional_templates"] = self.aditional_templates
			data_page["can_view_page"] = self.can_view_page
		
		data_page["js_files"] = self.js_files
		data_page["css_files"] = self.css_files
		try:
			data_page["last_error"] = request.session['last_error']
		except:
			data_page["last_error"] = ""
		
		request.session['last_error'] = ""
		return data_page
	
	def get(self, request, *args, **kwargs):
		"""
			Execute when the page makes a request
			@param request as request data
			@return render to response data
		"""
		self._preProccess(request)
		data_page = self.__initialProccess(request, kwargs)
		return render(request, self.template, data_page)
		
	
	def _preProccess(self, request):
		pass
	
	def post(self, request, *args, **kwargs):
		"""
			Proccess the post data
			@param request as request data
		"""	
		self._preProccess(request)
		success = False	
		if request.POST.get("draw", None) != None:
			dataRet = self._dataTable(request)
			return dataRet
			
		elif request.POST.get("ajaxRequest", None) != None:
			ajaxR = self._ajaxRequest(request)
			return ajaxR
		
		elif request.POST.get("getObjectData", None) != None:
			object_id = request.POST.get("object_id", None)
			if object_id != None:
				ajaxR = self._getObjectData(object_id)
				return ajaxR
			else:
				success = False	
		
		elif request.POST.get("tipoForm", None) != None:
			tipo = request.POST.get("tipoForm", None)
			
			if tipo == "editData":
				success = self._editData(request)
				return HttpResponseRedirect(str(request.path)+"?success="+str(success))
				
			elif tipo == "addData":
				success = self._addData(request)
				return HttpResponseRedirect(str(request.path)+"?success="+str(success))
				
			elif tipo == "deleteData":
				success = self._deleteData(request)
				return HttpResponseRedirect(str(request.path)+"?success="+str(success))
				
			elif tipo == "informeForm":
				return self._proccessInforme(request)
				
			else:
				self._nothigPost()
				
		else:
			self._nothigPost()		
		
		data_page = self.__initialProccess(request, kwargs)
		data_page["success"] = success
		return render(request, self.template, data_page)
	
	def _nothingPost(self):
		print("Nothing")
		
	def get_data(self, request, *args):
		"""
			Returns the data to show
			@param request as request data
			is abstract
		"""
		pass


class MainTable(Main):
	"""
		Main table class
	"""
	#### Tables definition elements
	template = "tablas.html"
	# Dict with Column user and Column back
	table_columns = dict(titulo = "Titulo")
	# Columns to return in _GetData
	return_edit_columns = []
	
	model_object = None
	register_name = ""
	form_action = "/index"
	mostrar_control_activos = False
	
	can_add = False
	can_edit = False
	can_delete = False
	form_edit = None
	form_add = None
	delete_register = False
		
	can_view = False
	view_aparte = False
	
	# Muestra el formulario en una página aparte, úsese sólamente cuando el registro tenga archivos
	edit_aparte = False
	
	def _checkPermission(self, object_id, user):
		return True
		
	def get_data(self, request, kwargs):
		"""
			Obtiene los datos predeterminados de la página
		"""
		editarGET = request.GET.get("EditRegister", None)
		addGET = request.GET.get("AddRegister", None)
		
		if (((editarGET != None and self.can_edit) or (addGET != None and self.can_add)) and self.edit_aparte):
			self._get_data_tables(request)
			self.template = "form.html"
			object_id = ""
			forms = []
			if editarGET != None and self.can_edit:
				object_id = request.GET.get("object_id", None)
				#if self._checkPermission(objeto_id, user):
				#	continue
				try:
					object_inst = self.model_object.objects.get(pk = object_id)
					tipo_form = "editData"
					forms.append(dict(href="first", tab="Editar registro", form = self.form_edit(instance = object_inst)))				
				except:
					tipo_form = "addData"
					forms.append(dict(href="first", tab="Añadir registro", form = self.form_add()))				
			elif addGET != None and self.can_add:
				tipo_form = "addData"
				forms.append(dict(href="first", tab="Añadir registro", form = self.form_add()))
			
			return_val = dict()
			return_val["register_name"] = self.register_name
			return_val["form_action"] = self.form_action
			return_val["forms"] = forms
			return_val["target"] = ""
			return_val["tipo_form"] = tipo_form
			return_val["object_id"] = object_id
			return return_val
		
		# Table Views
		if self.form_edit != None and self.edit_aparte == False and self.can_edit:
			self._add_aditional_template("varios/dialogo_editar.html")
			
		if self.form_add != None and self.edit_aparte == False and self.can_add:
			self._add_aditional_template("varios/dialogo_agregar.html")
		
		if self.delete_register and self.can_delete:
			self._add_aditional_template("varios/dialogo_eliminar.html")			
		
		# Add css and javascript files
		self._add_js("jquery.dataTables.min.js")
		self._add_js("dataTables.bootstrap4.min.js")
		self._add_js("tablas.js")
		self._add_css("dataTables.bootstrap4.min.css")
		self._add_css("dropdown_extension.css")
				
		self._get_data_tables(request)
		
		self.columns = [""]
		for key in self.table_columns.keys():
			self.columns.append(self.table_columns[key])
		self.columns.append("Operations")
		
		# Valores
		return_val = dict()
		return_val["register_name"] = self.register_name
		return_val["form_action"] = self.form_action
		return_val["columns"] = self.columns
		if self.form_edit != None:
			return_val["form_edit"] = self.form_edit()
		if self.form_add != None:
			return_val["form_add"] = self.form_add()
		return_val["mostrar_control_activos"] = self.mostrar_control_activos
		return_val["delete_register"] = self.delete_register
		#return_val["docente_add_delete"] = self.docente_add_delete
		return_val["can_add"] = self.can_add
		return_val["edit_aparte"] = self.edit_aparte
		return_val["success"] = request.GET.get("success", None)
		
		# return data
		return return_val
	
	def _editData(self, request):
		"""
			Edit the object data depending of the form
		"""
		object_id = request.POST.get("object_id", None)
		if object_id != None and self.can_edit:
			if self._checkPermission(object_id, request.user):
				sed = self.model_object.objects.get(pk = object_id)
				form_edit = self.form_edit(data = request.POST, files = request.FILES, instance=sed)
				if form_edit.is_valid():
					form_edit.save()	
					return True
			request.session['last_error'] = form_edit.errors
			#print(form_edit.errors)
		else:
			request.session['last_error'] = "Usted no tiene permisos para editar este objeto"
		return False
	
	def _addData(self, request):
		"""
			Add a table register
		"""
		if self.can_add:
			form_edit = self.form_edit(data = request.POST, files = request.FILES)
			if form_edit.is_valid():
				form_edit.save()
				return True
			request.session['last_error'] = form_edit.errors
		else:
			request.session['last_error'] = "Usted no tiene permisos para añadir este objeto"
		return False
	
	def _deleteData(self, request):
		"""
			Deletes a table register
		"""
		object_id = request.POST.get("object_id", None)
		if object_id != None and self.can_delete:
			if self._checkPermission(object_id, request.user):
				sed = self.model_object.objects.get(pk = object_id)
				sed.delete()
				return True
			request.session['last_error'] = "No se pudo eliminar el objeto"
		else:
			request.session['last_error'] = "Usted no tiene permisos para eliminar este objeto"
		return False
		
	
	def _ajaxRequest(self, request):
		"""
			Proccess the ajax data
		"""
		action = request.POST.get("ajax_action", None)
		error = False
		if action == "editar_modal":
			error = self._editData(request)
		if action == "agregar_modal":
			error = self._addData(request)
		if action == "eliminar_modal":
			error = self._deleteData(request)			
		error = not(error)		
		data = {
			'error': error
		}
		return JsonResponse(data)
	
	def _getObjectData(self, object_id):
		"""
			Returns the object data for complete the edit fields
			@param object_id as int
		"""
		object_data = self.model_object.objects.get(pk = object_id)
		data = {
			'object_id' : object_data.pk,
			'get_json' : True,
			'json_register_name' : str(object_data)
		}
		for key in self.return_edit_columns:
			if len(key) == 2 and type(key) in (tuple, list):
				value = str(getattr(getattr(object_data, key[0]), key[1]))
				key_item = key[0]
				data[key_item] = value
			else:
				data[key] = str(getattr(object_data, key))			
		return JsonResponse(data)
	
	
	def _dataTable(self, request):
		"""
			Return data of dataTable
			@param request as request data
		"""
		value = request.POST.get("search[value]", None)
		start = int(request.POST.get("start", 0))
		length = int(request.POST.get("length", 10))
		
		filters = ["pk"]
		for key in self.table_columns.keys():
			filters.append(key)
		filters.append("pk")
		
		order = int(request.POST.get("order[0][column]", len(filters)))
		add = ""
		direction = request.POST.get("order[0][dir]", 'desc')
		
		if direction == "desc":
			add = "-"
		
		fil = self._getFilerTable(value, request.user)
		
		solo_activos = request.POST.get("solo_activos", "true")		
		if solo_activos == "true":
			fil = fil & Q(estado = self.model_object.E_ACTIVO)
		
		if fil == Q():
			data = self.model_object.objects.all().order_by(add + filters[order])[start:start+length]
		else:
			data = self.model_object.objects.filter(fil).order_by(add + filters[order])[start:start+length]
			
		data_ret = []
		for e in data:
			datos = [
				"<input type='radio' class='input_check' name='col' value='" + str(e.pk) + "'>", 
			]
						
			for key in self.table_columns.keys():
				try:
					first, second = key.split("__")
					if second == "format":
						datos.append(self._getFormatRow(first, e, request.user))
					elif second == "date":						
						value = str(date_format(getattr(e, first), format='SHORT_DATETIME_FORMAT', use_l10n=False)).replace("None", "")
						datos.append(value)	
					else:
						value = str(getattr(getattr(e, first), second))
						datos.append(value)						
				except Exception as excep:
					print(excep)
					value = str(getattr(e, key))
					if os.path.isfile(settings.MEDIA_ROOT + "/" + value):
						direc = settings.MEDIA_URL + value
						datos.append("<a href='"+direc+"' download class='btn btn-xs btn-success'><i class='fa fa-download'></i>Descargar</a>")
					else:
						datos.append(value)				

			buttons = ""
			if self.form_edit != None and self.can_edit and self.can_edit_register(e, request.user):
				buttons = buttons + "<a title='Edit data' class='btn btn-xs btn-warning'"
				if self.edit_aparte == True:
					buttons = buttons + "href='" +self.form_action+ "?EditRegister&object_id="+str(e.pk)+"'"
				else:
					buttons = buttons + " href='#' onClick='editar_fila("+str(e.pk)+")'"
				buttons = buttons + "><i class='fa fa-edit'></i></a>"
			if self.can_view:
				if self.view_aparte:
					buttons = buttons + "<a title='View row' class='btn btn-xs btn-primary' href='"+str(self.form_action)+"view/"+str(e.pk)+"'><i class='fa fa-eye'></i></a>"
				else:
					buttons = buttons + "<a title='View row' class='btn btn-xs btn-primary' onClick='ver_fila("+str(e.pk)+")'><i class='fa fa-eye'></i></a>"
			if self.delete_register and self.can_delete and self.can_delete_register(e, request.user):
				buttons = buttons + "<button title='Delete row' class='btn btn-xs btn-danger' onClick='eliminar_fila("+str(e.pk)+")'><i class='fa fa-trash'></i></button>"
			datos.append(buttons)
			
			data_ret.append(datos)			
		return JsonResponse({'recordsTotal' : self.model_object.objects.all().count(), 'recordsFiltered' : len(data_ret), 'data' : data_ret})

	def can_edit_register(self, obj, user):
		return True
	def can_delete_register(self, obj, user):
		return True
	
