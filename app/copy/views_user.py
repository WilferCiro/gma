from .views_main import Main, MainTable
from django.views import View
from app.models import *

from django.db.models import Q
from django.conf import settings

from .form_user import *


class ProfileView(Main, View):
	#TODO: add historic
	template = 'profile.html'
	url = "/accounts/profile/"
	page_title = "My Profile"
	
	def get_data(self, request, kwargs):
		"""
			Gets extra data of the profile
		"""	
		self._add_breadcrumb("index", "/")
		self._add_breadcrumb("User profile", "")
		form_user = formUser(instance = request.user)
		return {"form_user":form_user}
	
	def _editData(self, request):
		"""
			Edit the object data depending of the form
		"""
		if request.POST.get("change_password", None) != None:
			current = request.POST.get("current_password", False)
			
			pass1 = request.POST.get("password1", "Pass1")
			pass2 = request.POST.get("password2", "Pass2")
			if pass1 == pass2:
				request.user.set_password(pass1)
				request.user.save()
				return True
			request.session['last_error'] = "Passwords don't match"
		else:
			form_edit = formUser(data = request.POST, instance=request.user)
			if form_edit.is_valid():
				form_edit.save()
				return True
			request.session['last_error'] = form_edit.errors
		return False
		
class UserViewProfile(Main, View):
	#TODO: add historic
	template = 'user/profile.html'
	url = "/users/view/"
	page_title = "User profile"
	
	def get_data(self, request, kwargs):
		try:
			user_id = int(kwargs['pk'])
			user_profile = User.objects.get(pk = user_id)
		except:
			user_profile = self.user
		
		user_data = {
			"username" : user_profile.username,
			"email" : user_profile.email,
			"cellphone" : user_profile.cellphone,
			"home_city" : user_profile.home_city,
			"profesion" : user_profile.profession,
			"full_name" : user_profile.get_full_name(),
			"pk" : user_profile.pk,
			"last_login" : user_profile.last_login,
			"creation_date" : user_profile.date_joined
		}
		
		self._add_breadcrumb("Home page", "/")
		self._add_breadcrumb("User profile", "")
		
		data_return = dict()
		data_return["user_data"] = user_data
		
		return data_return
		
class UserListView(MainTable, View):
	"""
		view of users list page
	"""
	page_title = "Users list"
	register_name = "user"	
	form_action = "/users/"	
	model_object = User
	table_columns = dict(last_name = "Last name", first_name = "First Name", email = "Email", profession = "Profession", locked = "locked?")
	return_edit_columns = ["last_name", "first_name", "email", "locked", "profession", "observations", "cellphone", "home_city", "home_country", "born_date"]
	form_edit = editUser
	form_add = addUser
	delete_register = True
	can_view = True
	view_aparte = True	
	
	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Home", "/")
		self._add_breadcrumb("Users list", "")
	
	def _addData(self, request):
		"""
			Add a table register
		"""
		if self.can_add:
			username = request.POST.get("username")
			try:
				exists = User.objects.get(username = username)
			except:
				new_user = User.objects.create_user(username=username,
		                             email=request.POST.get("email"))
				new_user.set_password(username)
				new_user.save()
				if new_user is not None:
					form_edit = addUser(request.POST, instance = new_user)
					if form_edit.is_valid():
						form_edit.save()
						return True
					print("error: " + str(form_edit.errors))
			request.session["last_error"] = "El usuario ya existe"		
		else:
			request.session['last_error'] = "Usted no tiene permisos para añadir este objeto"
		return False
	
	def _deleteData(self, request):
		"""
			Deletes a table register
		"""
		object_id = request.POST.get("object_id", None)
		if object_id != None and self.can_delete:
			if self._checkPermission(object_id, request.user):
				sed = self.model_object.objects.get(pk = object_id)
				try:
					for pu in ProjectUser.objects.filter(user = sed):
						pu.delete()
				except:
					pass
				sed.delete()
				return True
			request.session['last_error'] = "No se pudo eliminar el objeto"
		else:
			request.session['last_error'] = "Usted no tiene permisos para eliminar este objeto"
		return False
	
	def _checkPermission(self, object_id, user):
		if user.is_superuser:
			return True
		return False
	
	def _getFilerTable(self, value, user):
		fil = Q(is_superuser=False)			
		if value != "":
			fil = fil & (Q(last_name__icontains = value) | Q(first_name__icontains = value) | Q(email__icontains=value) | Q(profession__icontains = value) | Q(locked__icontains = value))
		return fil	
	
	def _preProccess(self, request):
		user = request.user
		if user.is_superuser:
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_view_page = False
	
		
		
				
		
