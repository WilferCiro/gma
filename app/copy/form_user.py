from django import forms
from .models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets                                       
from bootstrap_datepicker_plus import DateTimePickerInput, DatePickerInput
from django_select2.forms import Select2MultipleWidget, HeavySelect2Widget, HeavySelect2MultipleWidget


class formUser(ModelForm):
	
	class Meta:
		model = User
		fields = ["last_name", "first_name", "born_date", "cellphone", "email", "home_city", "home_country", "profession", "observations"]
		
		widgets = {
			'born_date': DatePickerInput(options={"format":"YYYY-MM-DD"}),
		}

class editUser(ModelForm):
	class Meta:
		model = User
		fields = ["last_name", "first_name", "born_date", "cellphone", "email", "home_city", "home_country", "profession", "locked"]
		
		widgets = {
			'born_date': DatePickerInput(options={"format":"YYYY-MM-DD"}),
		}
	
class addUser(ModelForm):
	class Meta:
		model = User
		fields = ["username", "last_name", "first_name", "born_date", "cellphone", "email", "home_city", "home_country", "profession", "locked"]
		
		widgets = {
			'born_date': DatePickerInput(options={"format":"YYYY-MM-DD"}),
		}
		
		
