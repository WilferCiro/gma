from django import forms
from .models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets                                       
from bootstrap_datepicker_plus import DateTimePickerInput, DatePickerInput
from django_select2.forms import Select2MultipleWidget, Select2Widget, HeavySelect2Widget, HeavySelect2MultipleWidget


class editProject(ModelForm):
	
	class Meta:
		model = Project
		fields = ["title", "observations", "keywords", "is_public"]
		
		widgets = {
			'observations': Textarea(attrs={'cols': 20, 'rows': 3}),
			'keywords': Textarea(attrs={'cols': 20, 'rows': 3}),
		}

class userAdd(forms.Form):
	user = forms.ChoiceField(
        widget=HeavySelect2Widget(
            data_url='/json/users'
        ), required = True
    )
	mode = forms.ChoiceField(choices = ProjectUser.MODE_CHOICES, required=True)

class SavedStates(forms.Form):
	saved_states = forms.ModelChoiceField(
		queryset = SavedStatesProblem.objects.none(),
        widget=Select2Widget(),
        required = True
    )
	def __init__(self, *args, **kwargs):
		project_id = kwargs.pop('project_id', None)
		super(SavedStates, self).__init__(*args, **kwargs)

		if project_id:
			self.fields['saved_states'].queryset = SavedStatesProblem.objects.filter(project = project_id)





