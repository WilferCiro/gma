from .views_main import Main
from django.views import View
from app.models import *
from django.contrib.auth import logout
from django.shortcuts import HttpResponseRedirect
from .form import *


class IndexPage(Main, View):
	"""
		view of index page
	"""
	template = 'index.html'
	page_title = "Home page"

	def get_data(self, request, kwargs):
		try:
			sed = IndexConfiguration.objects.last()
		except:
			sed = IndexConfiguration()
			sed.save()
			
		self._add_breadcrumb("Home", "")
		data_return = dict()
		data_return["configuration"] = sed
		data_return["project_number"] = Project.objects.all().count()
		data_return["public_projects_number"] = Project.objects.filter(is_public = "Public").count()
		return data_return


def logoutView (request):
	"""
		Logout from application, and returns to blog
	"""
	logout(request)
	return HttpResponseRedirect('/index/?success')

def loginView (request):
	"""
		Log in the page
	"""
	from django.contrib.auth import authenticate, login
	username = request.POST.get("username", None)
	password = request.POST.get("pwd", None)
	try:
		user = authenticate(username=username, password=password)
		login(request, user)
		return HttpResponseRedirect('/index/')
	except Exception as ex:
		print(ex)
		return HttpResponseRedirect('/index/?error')
		
		

class IndexConfig(Main, View):
	template = "form.html"
	page_title = "Edit index page"
	form_action = "/index/config/"
	
	def get_data(self, request, kwargs):
		
		self._add_breadcrumb("Home", "/")
		self._add_breadcrumb("Configuration", "")
		
		try:
			sed = IndexConfiguration.objects.last()
		except:
			sed = IndexConfiguration()
			sed.save()
		form_envio = editIndex(instance = sed)		
		
		data_return = dict()
		forms = [dict(href="first", completa="True", tab="Edit Data", form = form_envio)]
		data_return["forms"] = forms
		data_return["form_action"] = self.form_action
		data_return["tipo_form"] = "editData"
		data_return["target"] = ""
		return data_return
	
	def _editData(self, request):
		try:
			sed = IndexConfiguration.objects.last()
		except:
			sed = IndexConfiguration()
			sed.save()
		form_edit = editIndex(data = request.POST, files = request.FILES, instance=sed)
		if form_edit.is_valid():
			form_edit.save()	
			return True
		request.session['last_error'] = form_edit.errors
		return False
	

class SearchPage(Main, View):
	"""
		view of search page
	"""
	template = 'search.html'
	page_title = "Search results"

	def get_data(self, request, kwargs):
		busqueda = request.GET.get("q", "")
		full_items = []
		for men in self.menu:
			if busqueda.lower() in men["label"].lower() or busqueda.lower() in men["content"].lower():
				if men["url"] != "":
					full_items.append(men)				
			for item in men["submenu"]:
				if busqueda.lower() in item["label"].lower() or busqueda.lower() in item["content"].lower():
					full_items.append(item)
		
		projects = Project.objects.filter(Q(title__icontains = busqueda) & Q(is_public = Project.E_PUBLIC))[0:10]
		for pro in projects:
			data = dict(label = "Project: " + pro.title, url = "/projects/view/"+str(pro.pk), content = "Project Data")
			full_items.append(data)
		
		
		self._add_breadcrumb("Home", "/")
		self._add_breadcrumb("Search of \""+busqueda+"\"", "")
		return dict(query = busqueda, full_items = full_items)



