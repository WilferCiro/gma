from django import forms
from .models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets

class editIndex(ModelForm):
	
	class Meta:
		model = IndexConfiguration
		fields = "__all__"
