#Vista genérica para mostrar resultados
from django.views.generic.base import TemplateView
#Workbook nos permite crear libros en excel
from openpyxl import Workbook
from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from app.views.project import ProjectInputMatrixView, ProjectCrossMatrix
#Nos devuelve un objeto resultado, en este caso un archivo de excel
from django.http.response import HttpResponse
from django.shortcuts import HttpResponseRedirect

from app.models import *
#Nuestra clase hereda de la vista genérica TemplateView
class MainReport:
	pass

class ProjectReport(TemplateView, MainReport):
	
	def get(self, request, *args, **kwargs):
		user = request.user
		
		data_page = dict()
		if user.is_authenticated is False:
			return HttpResponseRedirect("/index/?success=false")
		
		pk = int(kwargs['pk'])
		try:
			project = Project.objects.get(pk = pk)
		except Exception as ex:
			self.can_view_page = False
			return HttpResponseRedirect("/index/?success=false")
			
		is_project_admin = False
		can_edit = False
		try:
			pu = ProjectUser.objects.get(user = request.user, project = project)
			if pu.mode == ProjectUser.ADMIN:
				is_project_admin = True
				can_edit = True
		except:
			if not project.is_public == Project.PUBLIC:
				self.can_view_page = False
				return HttpResponseRedirect("/index/?success=false")
		
		user_permissions = ProjectInputMatrixView._getUserParameter(self, request.user, project)
		
		wb = Workbook()
		
		wCross = wb.create_sheet("Cross consistency matrix", 0)
		wInput = wb.create_sheet("Input matrix", 0)
		wData = wb.create_sheet("Project data", 0)
		border = Side(border_style="thin", color="00827F")
		border_title = Side(border_style="double", color="00827F")
		
		## data putting
		
		wData['A1'] = "Data project"
		wData.merge_cells('A1:B1')
		wData['A2'] = 'Data'
		wData['B2'] = 'Value'
		
		wData['A3'] = 'Title: '
		wData['B3'] = project.title
		wData['A4'] = 'Visibility: '
		wData['B4'] = project.is_public
		wData['A5'] = 'Tags: '
		wData['B5'] = project.keywords
		wData['A6'] = 'Observations: '
		wData['B6'] = project.observations
		wData['A7'] = 'Modification date: '
		wData['B7'] = project.modification_date
		for i in ['A', 'B']:
			for a in range(2, 8):
				wData[str(i) + str(a)].border = Border(top=border, left=border, right=border, bottom=border)
		
		## Input matrix data
		table_model, parameter_model = ProjectInputMatrixView.getTableStructure(self, project, request.user, is_project_admin)
		
		column = 1
		for col in parameter_model:
			wInput.cell(row = 2, column = column).value = col["title"]
			wInput.cell(row = 2, column = column).border = Border(top=border_title, left=border_title, right=border_title, bottom=border_title)
			column += 1
		
		row = 3
		for col in table_model:
			column = 1
			for value in col:
				if value != 0:
					if value["exists"] == True:
						wInput.cell(row = row, column = column).value = value["title"]
						wInput.cell(row = row, column = column).border = Border(top=border, left=border, right=border, bottom=border)
				column += 1
			row += 1
		
		## Cross consistency matrix data
		table_model = ProjectCrossMatrix.getTableStructure(self, project)
		wCross.merge_cells(start_row = 1, start_column = 1, end_row = 2, end_column = 2)
		
		
		column = 3
		for title in table_model[-1]["titles_table"]:
			wCross.cell(row = 1, column = column).border = Border(top=border_title, left=border_title, right=border_title, bottom=border_title)
			wCross.cell(row = 1, column = column).value = title["title"]
			wCross.merge_cells(start_row = 1, start_column = column, end_row = 1, end_column = column + len(title["values"]) -1)
			for title2 in title["values"]:
				wCross.cell(row = 2, column = column).value = title2["title"]
				wCross.cell(row = 2, column = column).border = Border(top=border, left=border, right=border, bottom=border)
				column += 1
		
		numRow = 3
		for table in table_model:
			for row in table["intern_table"]:
				numCol = 2
				if row == table["intern_table"][0]:
					wCross.cell(row = numRow, column = 1).value = table["param_left"]
					wCross.cell(row = numRow, column = 1).border = Border(top=border_title, left=border_title, right=border_title, bottom=border_title)
					wCross.merge_cells(start_row = numRow, start_column = 1, end_row = numRow + len(table["intern_table"]) -1, end_column = 1)
				wCross.cell(row = numRow, column = numCol).value = row["title"]
				wCross.cell(row = numRow, column = numCol).border = Border(top=border, left=border, right=border, bottom=border)
				numCol += 1
				for cell in row["valores"]:
					#<td><button class="btn btn-success btn-sm stateButton" data-toggle="tooltip" data-placement="right"  pk="{{ cell.pk }}" title="{{cell.legend}}">{{ cell.value }}</button></td>
					wCross.cell(row = numRow, column = numCol).value = cell["value"]
					if cell["value"] == "X":
						wCross.cell(row = numRow, column = numCol).fill = PatternFill("solid", fgColor="00827F")
					else:
						wCross.cell(row = numRow, column = numCol).fill = PatternFill("solid", fgColor="00CE5B")
					wCross.cell(row = numRow, column = numCol).border = Border(top=border, left=border, right=border, bottom=border)
					numCol += 1
				numRow += 1
		
		## Download of file
		nombre_archivo ="ProjectData.xlsx" 
		response = HttpResponse(content_type="application/ms-excel") 
		contenido = "attachment; filename={0}".format(nombre_archivo)
		response["Content-Disposition"] = contenido
		wb.save(response)
		return response
