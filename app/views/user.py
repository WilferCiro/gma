from app.views.main import Main, MainTable
from django.views import View
from app.models import *

from django.db.models import Q
from django.conf import settings

from app.forms.user import *

		
class UserViewProfile(Main, View):
	#TODO: add historic
	template = 'user/profile.html'
	url = "/users/view/"
	page_title = "User profile"
	
	def get_data(self, request, kwargs):
		can_edit = False
		is_own_profile = False
		data_return = dict()
		try:
			user_id = int(kwargs['pk'])
			user_profile = User.objects.get(pk = user_id)
		except:
			user_profile = self.user
			is_own_profile = True
		
		if self.user.is_superuser or is_own_profile:
			can_edit = True
		
		if can_edit:
			data_return["form_edit_user"] = None
			data_return["form_edit_pass"] = None
			data_return["form_edit_avatar"] = editAvatar(instance = user_profile)
			data_return["form_edit_user"] = editUserProfile(instance = user_profile)
			data_return["form_change_pass"] = changePass()
		
		user_data = {
			"username" : user_profile.username,
			"email" : user_profile.email,
			"cellphone" : user_profile.cellphone,
			"born_date" : user_profile.born_date,
			"profesion" : user_profile.profession,
			"full_name" : user_profile.get_full_name(),
			"pk" : user_profile.pk,
			"last_login" : user_profile.last_login,
			"join_date" : user_profile.date_joined,
			"is_own_profile" : is_own_profile,
			"avatar" : user_profile.avatar
		}
		
		projsAdmin = []
		projsColl = []
		for pro in ProjectUser.objects.filter(user = user_profile):
			data = {
				"title" : pro.project.title,
				"modification_date" : pro.project.modification_date,
				"pk" : pro.project.pk
			}
			if pro.mode == ProjectUser.ADMIN:
				projsAdmin.append(data)
			else:
				projsColl.append(data)
		data_return["projects_collaborator"] = projsColl
		data_return["projects_admin"] = projsAdmin
		self._add_breadcrumb("Home page", "/")
		self._add_breadcrumb("User profile", "")
		
		data_return["user_data"] = user_data
		data_return["can_edit"] = can_edit
		
		return data_return
	
	def _editData(self, request):
		tipo_edit = request.POST.get("tipoEdit", None)
		object_id = request.POST.get("object_id", None)
		
		if tipo_edit == "user_data" and object_id != None:
			try:
				user_edit = User.objects.get(pk = object_id)
				if self.user.is_superuser or self.user == user_edit:
					form = editUserProfile(data = request.POST, instance = user_edit)
					if form.is_valid():
						form.save()
						return True
					self._add_error(form.errors)
			except Exception as e:
				print(e)
				pass
		
		elif tipo_edit == "user_avatar":
			try:
				user_edit = User.objects.get(pk = object_id)
				if self.user == user_edit:
					form = editAvatar(data = request.POST, files = request.FILES, instance = user_edit)
					if form.is_valid():
						form.save()
						return True
					self._add_error(form.errors)
			except Exception as e:
				print(e)
				pass
		
		elif tipo_edit == "password":
			try:
				user_edit = User.objects.get(pk = object_id)
				form_edit = changePass(data = request.POST)
				if form_edit.is_valid() and (self.user.is_superuser or self.user == user_edit):
					pass1 = form_edit.cleaned_data["pass1"]
					pass2 = form_edit.cleaned_data["pass2"]
					if pass1 == pass2:
						user_edit.set_password(pass1)
						user_edit.save()
						return True
					else:
						self._add_error("Las contraseñas no coinciden")
				else:
					self._add_error("No se puede editar la contraseña")
			except Exception as ex:
				print(ex)
		
		
		return False

	
	
		
class UserListView(MainTable, View):
	"""
		view of users list page
	"""
	page_title = "Users list"
	register_name = "user"	
	form_action = "/users/"	
	model_object = User
	table_columns = dict(last_name = "Last name", first_name = "First Name", email = "Email", profession = "Profession", locked = "locked?")
	return_edit_columns = ["last_name", "first_name", "email", "locked", "profession", "observations", "cellphone", "home_city", "home_country", "born_date"]
	form_edit = editUser
	form_add = addUser
	delete_register = True
	can_view = True
	view_aparte = True	
	
	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Home", "/")
		self._add_breadcrumb("Users list", "")
	
	def _addData(self, request):
		"""
			Add a table register
		"""
		if self.can_add:
			username = request.POST.get("username")
			try:
				exists = User.objects.get(username = username)
			except:
				new_user = User.objects.create_user(username=username,
		                             email=request.POST.get("email"))
				new_user.set_password(username)
				new_user.save()
				if new_user is not None:
					form_edit = addUser(request.POST, instance = new_user)
					if form_edit.is_valid():
						form_edit.save()
						return True
					print("error: " + str(form_edit.errors))
			request.session["last_error"] = "El usuario ya existe"		
		else:
			request.session['last_error'] = "Usted no tiene permisos para añadir este objeto"
		return False
	
	def _deleteData(self, request):
		"""
			Deletes a table register
		"""
		object_id = request.POST.get("object_id", None)
		if object_id != None and self.can_delete:
			if self._checkPermission(object_id, request.user):
				sed = self.model_object.objects.get(pk = object_id)
				try:
					for pu in ProjectUser.objects.filter(user = sed):
						pu.delete()
				except:
					pass
				sed.delete()
				return True
			request.session['last_error'] = "No se pudo eliminar el objeto"
		else:
			request.session['last_error'] = "Usted no tiene permisos para eliminar este objeto"
		return False
	
	def _checkPermission(self, object_id, user):
		if user.is_superuser:
			return True
		return False
	
	def _getFilerTable(self, value, user):
		fil = Q(is_superuser=False)			
		if value != "":
			fil = fil & (Q(last_name__icontains = value) | Q(first_name__icontains = value) | Q(email__icontains=value) | Q(profession__icontains = value) | Q(locked__icontains = value))
		return fil	
	
	def _preProccess(self, request):
		user = request.user
		if user.is_superuser:
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_view_page = False
	
		
		
				
		
