from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models import Q
from ckeditor.fields import RichTextField
from django.utils.formats import localize, date_format

class Country(models.Model):
	"""
		Defines the county model
	"""
	name = models.CharField(max_length = 50)
	description = models.TextField(null=True, blank=True)
	
	def __str__(self):
		return self.name

class User(AbstractUser):
	"""
		Defines a user model
		@extends of abstractuser
	"""
	creation_date = models.DateTimeField(auto_now_add = True, null=True, blank=True)
	
	G_MALE = "Male"
	G_FEMALE = "Female"
	SEX_CHOICES = (
		(G_MALE, 'Male'),
		(G_FEMALE, 'Female')
	)
	sex = models.CharField(
		max_length = 7,
		choices = SEX_CHOICES,
		null = True,
		blank = True
	)
	
	born_date = models.DateTimeField(null=True, blank=True)
	cellphone = models.IntegerField(default = 0, null=True, blank = True)
	home_city = models.CharField(max_length = 100, null=True, blank = True)
	home_country = models.ForeignKey(Country, on_delete=models.SET_NULL, null=True, blank=True)
	profession = models.CharField(max_length = 100, null=True, blank = True)
	observations = RichTextField(null=True, blank=True)
	avatar = models.ImageField(upload_to = 'ava/%s', null=True)
	LOCK_CHOICES = (
		("Active", "Active"),
		("Locked", 'Locked')
	)
	locked = models.CharField(
		max_length = 10,
		choices = LOCK_CHOICES,
		default = "Active",		
	)
	
	def __str__(self):
		return self.get_full_name()


class Project(models.Model):
	"""
		Defines the project model
	"""
	title = models.CharField(max_length = 100)
	observations = models.TextField(null=True, blank=True)
	creation_date = models.DateTimeField(auto_now_add = True)
	modification_date = models.DateTimeField(auto_now = True)
	
	E_PUBLIC = "Public"
	E_PRIVATE = "Private"
	
	PUBLIC_CHOICES = (
		(E_PUBLIC, 'Public'),
		(E_PRIVATE, 'Private')
	)
	is_public = models.CharField(
		max_length = 10,
		choices = PUBLIC_CHOICES,
		default = "Public",
	)
	keywords = models.TextField()
	deleted = models.BooleanField("Deleted", default=False)
	avatar = models.ImageField(upload_to = 'pro/%s', null=True, blank = True)
	
	def __str__(self):
		return self.title

class ProjectUser(models.Model):
	"""
		Defines the project-user model, assigns a user to a project as admin or collaborator
	"""
	ADMIN = 'Admin'
	COLLABORATOR = 'Collaborator'
	MODE_CHOICES = (
		(ADMIN, 'Admin'),
		(COLLABORATOR, 'Collaborator')
	)
	mode = models.CharField(
		max_length = 15,
		choices = MODE_CHOICES,
		default = ADMIN,
	)
	
	project = models.ForeignKey(Project, on_delete=models.CASCADE)
	user = models.ForeignKey(User, on_delete=models.PROTECT)
	observations = models.TextField(null=True, blank=True)
	edit_date = models.DateTimeField(auto_now = True)
	
	def __str__(self):
		return self.user.username + " - " + self.project.title
	
class Parameter(models.Model):
	"""
		Defines the parameter model
	"""
	title = models.TextField()
	edit_date = models.DateTimeField(auto_now_add = True)
	observations = models.TextField(null=True, blank=True)
	project = models.ForeignKey(Project, on_delete=models.CASCADE)
	order = models.IntegerField(default = 0)
	
	def __str__(self):
		return self.title

class ParameterProjectUser(models.Model):
	parameter = models.ForeignKey(Parameter, on_delete=models.CASCADE)
	project_user = models.ForeignKey(ProjectUser, on_delete=models.CASCADE)	
	M_SEE = 'See'
	M_EDIT = 'Edit'
	M_SEE_EDIT = 'SeeEdit'
	M_NONE = 'None'
	MODE_CHOICES = (
		(M_SEE, 'Only see'),
		(M_EDIT, 'Only edit'),
		(M_SEE_EDIT, 'See and edit'),
		(M_NONE, 'Nothing')		
	)
	mode = models.CharField(
		max_length = 15,
		choices = MODE_CHOICES,
		default = M_SEE_EDIT,
	)
	
	def __str__(self):
		return str(self.parameter) + " - " + str(self.mode) + " - " + str(self.project_user.user.get_full_name())
	

class Value(models.Model):
	"""
		Defines the value model
	"""
	title = models.TextField()
	edit_date = models.DateTimeField(auto_now_add = True)
	observations = models.TextField(null=True, blank=True)
	parameter = models.ForeignKey(Parameter, on_delete=models.CASCADE)
	order = models.IntegerField(default = 0)
	
	def __str__(self):
		return self.title
	

class CrossMatrix(models.Model):
	"""
		Defines the cross consistency matrix
	"""
	project = models.ForeignKey(Project, on_delete=models.CASCADE)
	
	STATE_CHOICES = (
		("-", '-'),
		("X", 'X')
	)
	state = models.CharField(
		max_length = 2,
		choices = STATE_CHOICES,
		default = "-",
	)
	
	observations = models.TextField(null=True, blank=True)
	edit_date = models.DateTimeField(auto_now_add = True)
	
	value1 = models.ForeignKey(Value, null=True, on_delete=models.CASCADE, related_name='value1')
	value2 = models.ForeignKey(Value, null=True, on_delete=models.CASCADE, related_name='value2')
	
	def __str__(self):
		return str(self.pk)


class ValuesCrossMatrix(models.Model):
	"""
		Relation of values with value cross matrix
	"""
	value = models.ForeignKey(Value, on_delete=models.CASCADE, null=True, blank=True)
	crossMatrix = models.ForeignKey(CrossMatrix, on_delete=models.CASCADE, related_name="crossmatrix")

	def __str__(self):
		return "Cross of " + str(self.value)
	

class SavedStatesProblem(models.Model):
	"""
		Saved states problem
	"""
	project = models.ForeignKey(Project, on_delete=models.CASCADE)
	model = models.TextField(null=True, blank=True) # Like 1, 2, 4, 6	
	name = models.CharField(max_length = 100, null=True, blank=True)
	observations = models.TextField(null=True, blank=True)
	edit_date = models.DateTimeField(auto_now = True, null = True, blank = True)
	
	def __str__(self):
		return str(self.name) + " - " + str(date_format(self.edit_date, format='SHORT_DATETIME_FORMAT', use_l10n=False))
		

class IndexConfiguration(models.Model):
	"""
		Configuration of index
	"""
	text = RichTextField(null=True, blank=True)
	edit_date = models.DateTimeField(auto_now = True, null = True, blank = True)
	
	def __str__(self):
		return "Configuration in " + str(self.edit_date)






