from django.urls import path
from app.views import project, user, others, ajax, reports

urlpatterns = [
	path('logout/', others.logoutView, name='Logout'),
	path('login/', others.loginView, name='Login'),
	
	path('index/', others.IndexPage.as_view(), name='IndexPageDV'),
	path('index/config/', others.IndexConfig.as_view(), name="IndexConfig"),
	path('', others.IndexPage.as_view(), name='IndexPageDV'),

	path('projects/', project.ProjectsView.as_view(), name='ProjectsView'),
	path('projects/view/<pk>', project.ProjectProfileView.as_view(), name='ProjectProfileView'),
	path('projects/input_matrix/<pk>', project.ProjectInputMatrixView.as_view(), name='ProjectInputMatrixView'),
	path('projects/cross_matrix/<pk>', project.ProjectCrossMatrix.as_view(), name='ProjectCrossMatrix'),
	path('projects/problem_states/<pk>', project.ProjectStatesView.as_view(), name='ProjectStatesView'),

	path('user/profile/', user.UserViewProfile.as_view(), name="ProfileView"),
	path('users/', user.UserListView.as_view(), name="UserListView"),
	path('users/view/<pk>', user.UserViewProfile.as_view(), name="UserViewProfile"),
	
	path("search/", others.SearchPage.as_view(), name="SearchPage"),
	
	path("report/project/<pk>", reports.ProjectReport.as_view(), name="ProjectReport"),

	path('json/users/', ajax.UserListAjax.as_view(), name="UserListAjax"),
]
