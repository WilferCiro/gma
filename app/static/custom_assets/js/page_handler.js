$(document).ready(function(){
	$("#generalLoading").hide();
});

class PageHandler {
	constructor(){
		self = this;
		self.configureAjax(self);
		self.DEBUG = true;
		$(".formSendAjax").submit({self : this}, this.submitFormAjax);
		
		//this.notifyLoad;
	}
	
	submitFormAjax (event) {
		event.preventDefault();
		var form = $(this);
		var url = form.attr("action");
		var self = event.data.self;
		var dataSend = form.serializeArray();
		dataSend.push({name : "ajaxRequest", value : true});
		self.sendData(self, dataSend, form, url, true);
	}
	
	
	getCookie(name) {
		var cookieValue = null;
		if(document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for(var i = 0; i < cookies.length; i++) {
			    var cookie = jQuery.trim(cookies[i]);
			    if(cookie.substring(0, name.length + 1) == (name + '=')) {
			        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
			        break;
			    }
			}
		}
		return cookieValue;
	}
	
	showLoading (){
		/*self.notifyLoad = new PNotify({
			title: 'Loading',
			text: 'Processing data, wait until ends.',
			type: 'info',
			addclass: 'notification-primary',
			hide: true,
		});*/
		//$("#generalLoading").show().removeClass("alert-success").removeClass("alert-danger").removeClass("alert-info").addClass("alert-info").text("Loading, Please wait...");
	}
	showError (error){	
		/*self.notifyLoad.remove();
		self.notifyLoad = new PNotify({
			title: 'Error',
			text: 'Your request has failed.',
			type: 'error',
			addclass: 'notification-danger',
			delay: 1500,
		});*/
		//$("#generalLoading").show().removeClass("alert-success").removeClass("alert-info").addClass("alert-danger").text("Error, processing");
	}
	showSuccess (){
		/*self.notifyLoad.remove();
		self.notifyLoad = new PNotify({
			title: 'Success',
			text: 'Your request has been successful.',
			type: 'success',
			addclass: 'notification-success',
			delay: 1500,
		});*/
		//$("#generalLoading").show().removeClass("alert-danger").removeClass("alert-success").removeClass("alert-info").addClass("alert-success").text("Success").hide(9000);
	}
	
	formLoading (form){
		form.find("#messageBody").text("Cargando...").removeClass("alert-danger").addClass("alert-primary");
	}
	
	formSuccess (json, form){
		if (json.error){
			self.formError(form);
		}
		else{
			form.find("#messageBody").text("Petición realizada con éxito");
			form.closest(".modal").modal("hide");
			form.trigger("reset");
			self.proccess_json(json, []);
		}
	}
	
	formError (form){
		form.find("#messageBody").text("Error al realizar la petición").removeClass("alert-primary").addClass("alert-danger");
	}
	
	configureAjax (self){
		$.ajaxSetup({
			global: true,
			beforeSend: function(xhr, settings) {
				if(!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
				    xhr.setRequestHeader("X-CSRFToken", self.getCookie('csrftoken'));
				    xhr.setRequestHeader("Content-Type", 'application/x-www-form-urlencoded; charset=UTF-8');
				}
			}
		});
	}
	
	sendData (self, data, array_objects, url, is_form = false){
		/*
			array_objects are the graphics elements to modify in proccess_json
			data is the dict that has the elements to send
		
		*/
		if (is_form){
			self.formLoading(array_objects);
		}
		else{
			self.showLoading();
		}
		$.ajax({
			url : url,
			data : data,
			type : "POST",
			dataType : 'json',
			success : function(json) {
				if (is_form){
					self.formSuccess(json, array_objects);
				}
				else{
					self.showSuccess();
					self.proccess_json(json, array_objects);
				}
			},
			error : function(xhr, status) {
				if (self.DEBUG){
					alert('Disculpe, ocurrió un problema' + xhr.responseText);
				}
				
				if (is_form){
					self.formError(array_objects);
				}
				else{
					self.showError(xhr.responseText);
				}
			}
		});

	}
	
	proccess_json (json_data, array_objects){
		
	}
}
