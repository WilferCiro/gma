$(document).ready(function(){
	const im = new problem_cross_matrix();
});


class problem_cross_matrix extends PageHandler {
	constructor() {
		super();
		$(".stateButton").click({self : this}, this.toggleState);
		$("#viewControlsCheck").change({self : this}, this.toggleControls);
		$(".formSendAjax").submit({self : this}, this.submitFormAjax);
		
		$(".btn-observations").click(self.editObservationsParameter);
		$("button, td, a").tooltip();
		
		var vare = $(".join_col");
		var last_col_span = "";
		for (var i = 0; i <= vare.length; i++){
			var col_span = $(vare[i]).attr("col_span");
			if (last_col_span != col_span){
				var cant = 1;
				var valors = $("[col_span='"+col_span+"']");
				cant = valors.length;
				$(vare[i]).attr("rowspan", cant);
			}
			else{
				$(vare[i]).remove();
			}
			last_col_span = col_span;
		}
		
	}
	
	toggleState (event){
		event.preventDefault();
		var self = event.data.self;
		var text = $(this).text();
		var pk = $(this).attr("pk")
		if (text == "-"){
			$(this).text("X");
			text = "X";
		}
		else{
			$(this).text("-");
			text = "-";
		}
		var project_id = $("#project_id").val();
		var url = $("#action").val();
		var data = {ajaxRequest : "Yes", ajax_action : "editData", tipoEdit: "state", state : text, cross_id : pk, project_id : project_id}
		self.sendData(self, data, [], url);
	}
	
	submitFormAjax (event) {
		event.preventDefault();
		var form = $(this);
		var self = event.data.self;
		var dataSend = form.serializeArray();
		self.sendData(self, dataSend, []);
	}
	
	editObservationsParameter(event) {
		var cross_id = $(this).attr("cross_id");
		var text = $(this).attr("observations");
		$("#edit-observations").modal("show");
		$("#observations_field").text(text);
		$("#cross_observations_id").attr("value", cross_id);
	}
	
	
	toggleControls (event){
		var checked = $(this).prop('checked');
		if (checked == true){
			$(".btn-observations").show(200);
			$("#viewControlsCheckLabel").text("Hide value controls");
		}
		else{
			$(".btn-observations").hide(200);
			$("#viewControlsCheckLabel").text("Show value controls");
		}
	}
	
}


