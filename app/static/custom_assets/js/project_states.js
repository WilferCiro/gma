$(document).ready(function(){
	const im = new project_states();
});


class project_states extends PageHandler {
	constructor() {
		super();
		$(".SelectableRow").click(this.rowSelect);
		this.valores = $("#preload_state").val();
		$("#save").click(this.save);
		$("#export").click(this.export_data);
		$("#load").click(this.load_data);
		
		this.loadedValues = undefined;
		
		$("#confirm_export").click(this.confirm_export);
		
		$("#OverwriteCheck").change(this.toggleOverwrite);
		$("#name_new").hide();
		
		$("#confirm-save").click(this.confirm_save);
		
		this.firstTime = false;
		this.send_request_rows();
	}
	
	save (event) {
		$("#save-state").modal('show');
	}
	
	export_data (event) {
		$("#export-state").modal("show");
	}
	
	load_data (event) {
		$("#load-state").modal("show");
	}
	
	toggleOverwrite (event) {
		
		var checked = $(this).prop('checked');
		if (checked == true){
			$("#name_new").hide("slow");
			$("#name_exists").show("slow");
		}
		else{
			$("#name_new").show("slow");
			$("#name_exists").hide("slow");
		}
	}
	
	confirm_export (event) {
		var format = $("#format_export").val();
		
		if (format == "PNG"){
			html2canvas($('#table_relational'), {
				onrendered: function(canvas) {

				    var saveAs = function(uri, filename) {
						var link = document.createElement('a');
						if (typeof link.download === 'string') {
							document.body.appendChild(link); // Firefox requires the link to be in the body
				            link.download = filename;
				            link.href = uri;
				            link.click();
				            document.body.removeChild(link); // remove the link when done
				        } else {
				            location.replace(uri);
				        }
				    };

				    var img = canvas.toDataURL("image/png"),
				        uri = img.replace(/^data:image\/[^;]/, 'data:application/octet-stream');

				    saveAs(uri, 'tableExport.png');
				}
			});
		}
	}
	
	rowSelect (event){
		
		if ($(this).hasClass("SelectableRow") || $(this).hasClass("OutputRow")){
			var pk = $(this).attr("value_id");
			var col_id = $(this).attr("col_id");
			self.valores += pk+",";
		}
		else if ($(this).hasClass("SelectedRow")){
			var pk = $(this).attr("value_id");
			var col_id = $(this).attr("col_id");
			self.valores = self.valores.replace(pk+",", "");
		}
		self.changeStates();
	}
	
	changeStates(event) {
		$(".tdRow").removeClass("InvalidRow");
		$(".tdRow").removeClass("SelectedRow");
		$(".tdRow").addClass("SelectableRow");
		
		if (self.valores != ""){
			$(".tdRow").addClass("InvalidRow");
			$(".tdRow").removeClass("SelectableRow");
			
			var valor_return = [];
			
			var valores_originales = self.valores.split(",");
			for (var a = 0; a < valores_originales.length; a++){
				var valores_internos = [];
				if (valores_originales[a] != ""){
					for (i = 0; i < self.loadedValues.length; i++){
						if (self.loadedValues[i].value1 == valores_originales[a]){
							valores_internos.push(self.loadedValues[i].value2);
						}
						else if(self.loadedValues[i].value2 == valores_originales[a]){
							valores_internos.push(self.loadedValues[i].value1);
						}
					}
				}
				if (valores_internos.length > 0){
					valor_return.push(valores_internos);
				}
			}
			
			var definitivos = [];
			if(valor_return.length > 1){
				for (var i = 0; i < valor_return.length; i++){
					var valor = valor_return[i];
					for (var ii = 0; ii < valor.length; ii++){
						var error = false;
						for (var a = 0; a < valor_return.length; a++){
							if (valor_return[a].indexOf(valor[ii]) == -1){
								error = true;
							}
						}
						if (error == false && definitivos.indexOf(valor[ii]) == -1){
							definitivos.push(valor[ii]);
						}
					}
				}
			}
			else{
				definitivos = valor_return[0];
			}
			for (var i = 0; i <= definitivos.length; i++){
				if (definitivos[i] != ""){
					$("[item='item_"+definitivos[i]+"']").removeClass("InvalidRow").addClass("SelectableRow");
				}
			}
		}
		var valores_originales2 = self.valores.split(",");
		for (var a = 0; a < valores_originales2.length; a++){
			if (valores_originales2[a] != ""){
				$("[item='item_"+valores_originales2[a]+"']").removeClass("InvalidRow").addClass("SelectedRow");
			}
		}
	}
	
	send_request_rows() {
		var project_id = $("#project_id").val();
		var data = {ajaxRequest : "Yes", ajax_action : "requestData", pk : self.valores, project_id : project_id}
		var url = $("#form_for_action").val();
		self.sendData(self, data, "data_click", url);
		
	}
	
	confirm_save (event){
		var over = $("#OverwriteCheck").prop('checked');
		var project_id = $("#project_id").val();
		if (over == false){
			var new_name = $("#new_name").val();
			var action = "save_new";
		}
		else{
			var new_name = $("#form_save").find("#id_saved_states").val();
			var action = "save_over";
		}
		
		var data = {ajaxRequest : "Yes", ajax_action : "editData", pk : self.valores, project_id : project_id, action : action, new_name : new_name}
		var url = $("#form_for_action").val();
		self.sendData(self, data, "save", url);
	
	}
	
	proccess_json (json_data, object_click){
		if (object_click == "save"){
			window.location.href = "?saved_states="+json_data.pk
		}
		else if(object_click == "data_click"){
			self.loadedValues = json_data.values[0];
			if (self.firstTime == false){
				self.changeStates();
				self.firstTime = true;
			}
		}
		
	}
	
}
