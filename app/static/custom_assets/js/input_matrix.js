$(document).ready(function(){
	const im = new input_m();
});


class input_m extends PageHandler {
	constructor() {
		super();
	
		$(".deleteValue").click(this.deleteValue);
		$(".deleteParameter").click(this.deleteParameter);
		
		$(".formSendAjax").submit({self : this}, this.submitFormAjax);
		
		$("button, div.value").tooltip();
		
		$(".addValueButton").click(self.addValueModal);
		
		$(".edit_permission").click(self.edit_permission);
		
		$(document).keyup(function(e) {
			if (e.keyCode === 27) self.returnDefault();
		});
		$(".bg-float").click(function(e) {
			self.returnDefault();
		});
		$(".click_row").click(function(){
			$(this).parent().addClass("selected");
			$(".visible-box").removeClass("visible-box");
			$(this).parent().find(".flotante-box").addClass("visible-box");
			$(".bg-float").addClass("visible-box");
			$(this).parent().find(".flotante-box").find(".controlTextarea").focus();
		});
		$('.close_view').click(function(){
			self.returnDefault();
		});
		
		
	}
	
	returnDefault (event) {
		$(".visible-box").removeClass("visible-box");
		$(".selected").removeClass("selected");		
	}
	
	addValueModal(event) {
		var parameter_id = $(this).attr("parameter_id");
		$("#add-value").modal("show");
		$("#parameter_id_add_value").attr("value", parameter_id);
	}
	
	deleteParameter (event) {
		var parameter_id = $(this).attr("parameter_id");
		$("#parameter_id_delete").attr("value", parameter_id);
		$("#tipoDelete").attr("value", "parameter");
		$('#delete-model-modal').modal('show');
	}
	
	deleteValue (event) {
		var parameter_id = $(this).attr("parameter_id");
		var value_id = $(this).attr("value_id");
		$("#parameter_id_delete").attr("value", parameter_id);
		$("#value_id_delete").attr("value", value_id);
		$("#tipoDelete").attr("value", "value");
		$('#delete-model-modal').modal('show');
	}
		
	edit_permission (event){
		event.preventDefault();
		var pk = $(this).attr("pk");
		var can_view = $("#can_view"+pk).prop('checked');
		var can_edit = $("#can_edit"+pk).prop('checked');
		var project_id = $("#project_id").val();
		var data = {ajaxRequest : "Yes", ajax_action : "editData", tipoEdit: "permission", can_view : can_view, can_edit : can_edit, pk : pk, project_id : project_id}
		var url = $("#form_for_action").attr("action");
		self.sendData(self, data, [], url);
	}
		
	
}


