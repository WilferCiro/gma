$(document).ready(function(){
	const im = new project_states();
});


class project_states extends PageHandler {
	constructor() {
		super();
		$(".SelectableRow").click(this.rowSelect);
		this.valores = $("#preload_state").val();
		$("#save").click(this.save);
		$("#export").click(this.export_data);
		$("#load").click(this.load_data);
		
		$("#confirm_export").click(this.confirm_export);
		
		$("#OverwriteCheck").change(this.toggleOverwrite);
		$("#name_new").hide();
		
		$("#confirm-save").click(this.confirm_save);
		
		this.send_request_rows();
	}
	
	save (event) {
		$("#save-state").modal('show');
	}
	
	export_data (event) {
		$("#export-state").modal("show");
	}
	
	load_data (event) {
		$("#load-state").modal("show");
	}
	
	toggleOverwrite (event) {
		
		var checked = $(this).prop('checked');
		if (checked == true){
			$("#name_new").hide("slow");
			$("#name_exists").show("slow");
		}
		else{
			$("#name_new").show("slow");
			$("#name_exists").hide("slow");
		}
	}
	
	confirm_export (event) {
		var format = $("#format_export").val();
		
		if (format == "PNG"){
			html2canvas($('#table_relational'), {
				onrendered: function(canvas) {                                      

				    var saveAs = function(uri, filename) {
				        var link = document.createElement('a');
				        if (typeof link.download === 'string') {
				            document.body.appendChild(link); // Firefox requires the link to be in the body
				            link.download = filename;
				            link.href = uri;
				            link.click();
				            document.body.removeChild(link); // remove the link when done
				        } else {
				            location.replace(uri);
				        }
				    };

				    var img = canvas.toDataURL("image/png"),
				        uri = img.replace(/^data:image\/[^;]/, 'data:application/octet-stream');

				    saveAs(uri, 'tableExport.png');
				}
			});
		}
	}
	
	rowSelect (event){
		if ($(this).hasClass("SelectableRow") || $(this).hasClass("OutputRow")){
			var pk = $(this).attr("value_id");
			var col_id = $(this).attr("col_id");
			
			self.valores += pk+",";
			self.send_request_rows();
		}
		else if ($(this).hasClass("SelectedRow")){
			var pk = $(this).attr("value_id");
			var col_id = $(this).attr("col_id");
			
			self.valores = self.valores.replace(pk+",", "");
			self.send_request_rows();
		}
	}
	
	send_request_rows() {
		var project_id = $("#project_id").val();
		var data = {ajaxRequest : "Yes", ajax_action : "requestData", pk : self.valores, project_id : project_id}
		var url = $("#form_for_action").val();
		self.sendData(self, data, [], url);
	}
	
	confirm_save (event){
		var over = $("#OverwriteCheck").prop('checked');
		var project_id = $("#project_id").val();
		if (over == false){
			var new_name = $("#new_name").val();
			var action = "save_new";
		}
		else{
			var new_name = $("#form_save").find("#id_saved_states").val();
			var action = "save_over";
		}
		
		var data = {ajaxRequest : "Yes", ajax_action : "editData", pk : self.valores, project_id : project_id, action : action, new_name : new_name}
		var url = $("#form_for_action").val();
		self.sendData(self, data, "save", url);	
	
	}
	
	proccess_json (json_data, object_click){
		if (object_click == "save"){
			window.location.href = "?saved_states="+json_data.pk
		}
		else if(object_click == "data_click"){
			
		}
		else{
			$(".tdRow").removeClass("InvalidRow");
			$(".tdRow").removeClass("SelectedRow");
			$(".tdRow").addClass("SelectableRow");
			
			if (self.valores != ""){
				$(".tdRow").addClass("InvalidRow");
				$(".tdRow").removeClass("SelectableRow");
				var valor_return = (json_data.values).split(",");
				for (var i = 0; i <= valor_return.length; i++){
					if (valor_return[i] != ""){
						$("[item='item_"+valor_return[i]+"']").removeClass("InvalidRow").addClass("SelectableRow");
					}
				}
			}
			var valores_originales = self.valores.split(",");
			for (var a = 0; a < valores_originales.length; a++){
				if (valores_originales[a] != ""){
					$("[item='item_"+valores_originales[a]+"']").removeClass("InvalidRow").addClass("SelectedRow");
				}
			}
		}
		
	}
	
}
